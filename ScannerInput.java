package org.oop.tugas1;

import java.util.Scanner;

public class ScannerInput {
     public static void main (String[] args){
        Scanner input = new Scanner(System.in);
        String result;
        int num, again = 1;
        
        do {
            System.out.print("Masukkan angka: ");
            num = input.nextInt();
            result = "Angka " + num + " merupakan bilangan " + ((num % 2 == 0) ? "Genap" : "Ganjil");
            System.out.println(result);
            System.out.println("Ulangi? 1. Ya 2. Tidak");
            again = input.nextInt();
        } while (again == 1);
    }
}
